const fs = require('fs');
const path = require('path');
const express = require('express');
const router = express.Router();

const folderPath = './files/';


router.post('/', async (req, res, next) => {
    try{
        const fileName = req.body.filename
        if(!req.body.content){
            res.status(400).send({'message': "Please specify 'content' parameter"});
        } else if(!req.body.filename){
            res.status(400).send({'message': "Please specify 'filename' parameter"});
        } else if(!fileName.match(/[^:\/\\ /|/*\?><\"]*[\.](log|txt|json|yaml|xml|js|)$/)){
            res.status(400).send({'message': "Program supports only log|txt|json|yaml|xml|js extensions"});
        } else{
            fs.writeFile(path.join(__dirname, folderPath, `${req.body.filename}`), `${req.body.content}`, (err) => {
            if (err) {
                next(err)
            } else{
                res.status(200).send({ 'message': 'File created successfully' });
            }})
        }
    } catch{
    console.log(error)
    next(err)
    }
});

router.get('/', async (req, res, next) => {
    fs.readdir(folderPath, (err, files) => {
        if (!err){
        let list = new Array();
        files.forEach(file => {
            list.push(file);
        });
        res.status(200).send({
            "message": "Success",
            "files": list
        })
        } else{
            console.log(error)
            next(err)
        }
    });
});

  
router.get('/:filename',  async (req, res, next)  => {
    try{
        const fileName = req.params.filename    
        fs.readFile(folderPath + fileName, 'utf8', function(err, contents) {
            if (err) {
              res.status(400).send({'message': `No file with '${fileName}' filename found`})
            } else{
                let {date} = fs.statSync(`${folderPath + fileName}`)
                let extension = fileName.split('.').pop();
                res.send({"message":"Success",
                "filename": `${fileName}`,
                "content": `${contents}`,
                "extension": `${extension}`,
                "uploadedDate": `${date}`
                }) 
            }
        })}catch (error) {
          console.log(error)
          next(err)
      }   
})

module.exports = {
    filesRouter: router
}