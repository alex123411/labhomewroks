const fs = require('fs');
const path = require('path');
const express = require('express');
const morgan = require('morgan')
const app = express();

const {filesRouter} = require('./controller.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', filesRouter)

const start = async () => {
  try {
      // await mongoose.connect('mongodb+srv://testuser:testuser1@cluster0.j9zkc.mongodb.net/test?retryWrites=true&w=majority', {
      //     useNewUrlParser: true, useUnifiedTopology: true
      // });
      if (!fs.existsSync('files')) {
        fs.mkdirSync('files')
      }
      app.listen(8080);
  } catch (err) {
      console.error(`Error on server startup: ${err.message}`);
  }
}

start();

//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  console.log(err)
  res.status(500).send({'message': 'Server error'});
}

